__version__ = '1.0.0'

from .constants import DEFAULT_MODS_PATH, DEFAULT_XCOM2_WORKSHOP_PATH, XCOM2_MOD_EXTENSION, XCOM2_APP_ID
from .synchronizer import Synchronizer
from .command import main
