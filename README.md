# xcom2modsync - XCOM2 Mod Synchronizer

Install Steam Workshop mods into XCOM 2 mod directory.

## Problem

Linux version of XCOM2 is currently ignoring mods installed via Steam Workshop. Although they show up in the Mod Manager
that comes with the game, they are not applied to the game.

Only workaround for this so far is to copy (symbolic links do not work) the mod directories from workshop
(`~/.local/share/Steam/steamapps/workshop/content/268500`) to the game's mod directory
(`~/.local/share/Steam/steamapps/common/XCOM 2/share/data/xcomgame/mods`) renaming them to lowercase names of the mods. 

Thanks [LCG](http://steamcommunity.com/app/268500/discussions/0/412446890551909270/#c412446890554027672)

## Solution

`xcom2modsync` is a command-line tool for synchronizing XCOM2 mods installed via Steam workshop with the game's mods directory.

### Requirements

* Python3
* Virtualenv (Optional)

### Installation

Simply install it from [PyPI](https://pypi.python.org/pypi?:action=display&name=xcom2modsync)

```bash
pip install xcom2modsync
```
*If you're* **not** *using a virtualenv then you might have to prefix this with `sudo`*

### Usage

Subscribe to a workshop mod that you'd like to use

![Before](https://gitlab.com/nCrazed/XCOM2-Mod-Synchronizer/raw/master/docs/before.png)

Run the command manually
```bash
xcom2modsync
```
*Make sure to activate the virtualenv if you used one during installation.*

This command will look through your XCOM2 Workshop directory for directories containing `*.XComMod` file and install them
into the game's mods directory. In order to keep the mods in sync with the workshop version it will delete the installed
mod directory if one already exists before copying.

After running this command, simply start the game and enable the non-workshop versions of the mods. The names will have
lower case letters and no spaces.

![After](https://gitlab.com/nCrazed/XCOM2-Mod-Synchronizer/raw/master/docs/after.png)

You can automate this process by adding `xcom2modsync` to the game's launch script: 

```bash
#/home/edvin/.steam/steam/steamapps/common/XCOM 2/XCOM2.sh
...
xcom2modsync
"${GAMEROOT}/bin/XCOM2" $@$
...
```